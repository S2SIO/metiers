% Les métiers du développement
% par [moulinux](https://moulinux.frama.io/)
% (révision 07.09.2020)

Objectifs : *Découvrir les métiers liés au BTS SIO et le bassin
d'emplois, au travers de la rédaction d’une fiche métier en
correspondance avec la formation proposée à partir des ressources mises
à votre disposition par vos enseignants. L’accès à Internet vous
permettra de compléter celles-ci*.

# Production attendue

## Fiche métier

Cette fiche métier devra rester synthétique. Elle sera livrée au **format**
[markdown](https://fr.wikipedia.org/wiki/Markdown). Cette fiche devra
comporter différentes informations :

-   Un descriptif détaillant ce métier (activités, missions,
    Savoir-être, qualités, etc);
-   Vous préciserez si ce métier est directement accessible après votre
    formation ou si des études complémentaires sont nécessaires (niveau d'études requis);
-   Une liste de technologies liées à ce métier (vous essaierez d'organiser ces technologies
    en différentes catégories et aussi de distinguer les technologies libres des autres) ;
-   Les organisations du bassin d’emploi *local* (au moins trois) que
    vous avez identifié comme offrant une telle opportunité pour ce
    métier : précisez les noms, adresses et activités ; *évitez les
    cabinets de recrutement* ! Vous **justifierez** ces choix par
    des extraits de leur site Web;
-   Vos *sources* : c'est à dire les références qui vous ont été
    nécessaires pour remplir votre fiche (vous porterez une note de
    pertinence sur 5 pour chaque source selon vos propres critères que
    vous pourrez détailler).

Vous vous inspirerez notamment de la [fiche] « Enseignant en
informatique », que ce soit pour l'organisation de la fiche ou comme
base de départ pour les technologies.

> Cette fiche métier est un document préalable pour réaliser la mission suivante...

Vous vous appuierez également sur les ressources fournies en annexe.


## Éditeur

Markdown est un parmi de nombreux [langages de
balisage](https://fr.wikipedia.org/wiki/Langage_de_balisage_léger),
permettant de formater simplement un texte. Markdown a notamment gagné
en notoriété en étant supporté par de nombreuses [forges
logicielles](https://fr.wikipedia.org/wiki/Forge_(informatique)).

De nombreux logiciels permettent d’éditer des fichiers au format
markdown. La plupart des éditeurs proposent une extension ad hoc. Certains
éditeurs dédiés existent même comme l'application mono-utilisateur ReText,
ou l'éditeur collaboratif [CodiMD](https://github.com/hackmdio/codimd).

Le wiki des dépôts framagit (propulsé par le logiciel libre
[gitlab-ce](https://fr.wikipedia.org/wiki/GitLab_CE)) repose sur la
[syntaxe markdown][markdown]. Vous pourrez intégrer la version finalisée et validée
par votre enseignant, une fois que votre compte personnel aura été intégré au projet.

> **Remarque** : Les fichiers au format markdown pourront ensuite être
traités pour créer des diaporamas ou mis en page dans d’autres formats
(PDF, etc). La configuration de l’habillage sera fera notamment en
utilisant des feuilles de styles que nous découvrirons ultérieurement...


# Les métiers du développeur

Afin d’approfondir vos recherches (voir les annexes), vous pouvez
utiliser le [moteur de recherche Qwant](https://www.qwant.com/), qui en
plus d’être [respectueux de vos données](https://about.qwant.com/fr/legal/confidentialite/),
présente des réponses pertinentes, par défaut, en français...

## Introduction

Le site [LinuxJobs](https://www.linuxjobs.fr/), résume assez bien les
différentes catégories de recherche :

* Programmeur
* Bases de données
* Devops

La dernière catégorie :
[DevOps](https://fr.wikipedia.org/wiki/Devops) concerne des emplois qui
requiert des connaissances communes aux deux parcours (développeurs et réseaux)
de la formation SIO. Ces métiers nécessitent également une formation
complémentaire.

> Les métiers de la sécurité ne constituent pas encore une catégorie à part sur ce site.

## Métiers à explorer

En travaillant en binôme, vous essaierez de répondre aux objectifs de la
mission en vous orientant sur un des métiers suivants :

1. Développeur / Développeuse *front-end* ;
2. Développeur / Développeuse *back-end* ;
3. Développeur / Développeuse d'applications bureautiques, multi-plateformes ;
4. Développeur / Développeuse d’applications mobiles ;
5. Administrateur / Administratrice de base de données ;
6. Testeur / Testeuse d’applications ;
7. Webmestre
8. *DevOps* ;

Les binômes seront définis par votre enseignant.

> Les métiers de la sécurité seront abordés via les spécialisations
évoquées ci-dessus.


# Les entreprises du développeur

Découvrir les organisations locales est important pour votre recherche de
stage.

## Introduction

Un technicien informatique peut exercer son activité dans différents cadres :

* Entité informatique interne à une organisation (entreprise privée, entreprise publique, collectivité, etc) ;
* [Entreprise de services du numérique][esn] ;
* Société de conseil en technologies ;
* Éditeur de logiciels informatiques.

> Les rôles qui lui seront attribués vont dépendre notamment de la taille de l'organisation.

## Les organisations locales

La consultation de l'[annuaire](http://www.vichy-economie.com/annuaire-economique/#/annuaire) de
Vichy Val d'Allier (également sous forme papier) est un bon
moyen pour débuter. Par exemple en consultant la rubrique : « Services
aux entreprises », puis la sous-rubrique : « Informatique et
Télécommunication », mais également la rubrique Industrie (pour les
entreprises de plus de 50 salariés qui peuvent avoir des services informatiques).

> **Important** : Prenez garde cependant à éviter les auto-entrepreneurs et donc à bien lire l'information *effectif*, car ce genre d'expériences ne vous aidera guère à trouver un emploi après le BTS : essayez de trouver un stage là où vous aimeriez travailler plus tard : l'auto-entrepreneur travaille pour son compte, il ne va pas créer votre emploi !

> Évitez également les agences de communication qui ne disposent généralement pas de compétences informatiques (elles peuvent faire appel à des travailleurs *freelance* en cas de nécessité).

## Les organisations régionales

Élargir votre champ de recherche pour le stage est forcément la bonne
idée. Il n'y a pas que les entreprises qui accueillent des stagiaires !


# Annexes

## Ressources vidéos

### Dev

* [Développeur / développeuse informatique](http://www.onisep.fr/Ressources/Univers-Metier/Metiers/developpeur-developpeuse-informatique)
* [Développeur / développeuse web](https://www.oriane.info/metier/developpeur/415)
* Développeur / développeuse informatique (java) : [invidious](https://invidious.fdn.fr/watch?v=5mH1QfeK7Z8&autoplay=0&continue=0&dark_mode=true&hl=fr&listen=0&local=1&loop=0&nojs=0&player_style=youtube&quality=dash&thin_mode=false), [youtube](https://www.youtube.com/watch?v=5mH1QfeK7Z8)
* Développeur - Développeuse Web (agilité, lead) : [invidious](https://invidious.fdn.fr/watch?v=mP_fnttJ5g0&autoplay=0&continue=0&dark_mode=true&hl=fr&listen=0&local=1&loop=0&nojs=0&player_style=youtube&quality=dash&thin_mode=false), [youtube](https://www.youtube.com/watch?v=mP_fnttJ5g0)
* OH MY JOB - Développeur Full Stack : [invidious](https://invidious.fdn.fr/watch?v=kZ6ygqZAhl4&autoplay=0&continue=0&dark_mode=true&hl=fr&listen=0&local=1&loop=0&nojs=0&player_style=youtube&quality=dash&thin_mode=false), [youtube](https://www.youtube.com/watch?v=kZ6ygqZAhl4)
* Développeur Application Mobile: [invidious](https://invidious.fdn.fr/watch?v=ArTkROCUSIw&autoplay=0&continue=0&dark_mode=true&hl=fr&listen=0&local=1&loop=0&nojs=0&player_style=youtube&quality=dash&thin_mode=false), [youtube](https://www.youtube.com/watch?v=ArTkROCUSIw)

### Base de données

* Administratrice Base de données - Des Métiers pour l'informatique : [invidious](https://invidious.fdn.fr/watch?v=Fc0sibzSlwo&autoplay=0&continue=0&dark_mode=true&hl=fr&listen=0&local=1&loop=0&nojs=0&player_style=youtube&quality=dash&thin_mode=false), [youtube](https://www.youtube.com/watch?v=Fc0sibzSlwo)
* Administrateur de bases de données / Administratrice de bases de données (mysql): [invidious](https://invidious.fdn.fr/watch?v=pvVWqQTX3OQ&autoplay=0&continue=0&dark_mode=true&hl=fr&listen=0&local=1&loop=0&nojs=0&player_style=youtube&quality=dash&thin_mode=false), [youtube](https://www.youtube.com/watch?v=pvVWqQTX3OQ)

### Cybersécurité

* Zoom sur les métiers de la Cybersécurité: [invidious](https://invidious.fdn.fr/watch?v=GVv5WFePFCM&autoplay=0&continue=0&dark_mode=true&hl=fr&listen=0&local=1&loop=0&nojs=0&player_style=youtube&quality=dash&thin_mode=false), [youtube](https://www.youtube.com/watch?v=GVv5WFePFCM)
* Jobs de Demain : Pentester : [invidious](https://invidious.fdn.fr/watch?v=IqZtV5h8lxA&autoplay=0&continue=0&dark_mode=true&hl=fr&listen=0&local=1&loop=0&nojs=0&player_style=youtube&quality=dash&thin_mode=false), [youtube](https://www.youtube.com/watch?v=IqZtV5h8lxA)
* Qu’est-ce qu’un SOC - Security Operations Center ? 5 min pour comprendre: [invidious](https://invidious.fdn.fr/watch?v=Awsajwvlris&autoplay=0&continue=0&dark_mode=true&hl=fr&listen=0&local=1&loop=0&nojs=0&player_style=youtube&quality=dash&thin_mode=false), [youtube](https://www.youtube.com/watch?v=Awsajwvlris)

### DevOps

* DevOps, qu'est-ce que c'est: [invidious](https://invidious.fdn.fr/watch?v=TZBVWCpSNvc&autoplay=0&continue=0&dark_mode=true&hl=fr&listen=0&local=1&loop=0&nojs=0&player_style=youtube&quality=dash&thin_mode=false), [youtube](https://www.youtube.com/watch?v=TZBVWCpSNvc)
* Le DevOps expliqué en emojis: [invidious](https://invidious.fdn.fr/watch?v=M6F6GWcGxLQ&autoplay=0&continue=0&dark_mode=true&hl=fr&listen=0&local=1&loop=0&nojs=0&player_style=youtube&quality=dash&thin_mode=false), [youtube](https://www.youtube.com/watch?v=M6F6GWcGxLQ)

## Autres ressources métiers sur Internet

Des témoignages d'anciens étudiants de la formation sur leurs parcours
sont listés sur ce [projet](https://framagit.org/promotions/temoignages) (accès privé réservé aux promotions).

Les [fiches métiers](https://www.oriane.info/recherche/metier) (voir les
vidéos partagées ci-dessus) avec notamment : les fiches
[Développeur(euse)](https://www.oriane.info/metier/developpeur/415),
[Développeur(euse) d’applications mobiles](https://www.oriane.info/metier/developpeur-d’applications-mobiles/414),
[Testeur(euse)](https://www.oriane.info/metier/testeur/522),
[Webmestre](https://www.oriane.info/metier/webmestre/265),
[Intégrateur(trice) Web](https://www.oriane.info/metier/integrateur-web/272),
[Administrateur de bases de données](https://www.oriane.info/metier/administrateur-de-bases-de-donnees/341).

L'Onisep propose également des fiches métier :
[Développeur(euse)](http://www.onisep.fr/Ressources/Univers-Metier/Metiers/developpeur-developpeuse-informatique),
[Webmestre](http://www.onisep.fr/Ressources/Univers-Metier/Metiers/webmestre),
[Testeur(euse) en informatique](http://www.onisep.fr/Ressources/Univers-Metier/Metiers/testeur-testeuse),
[La Cybersécurité](http://www.onisep.fr/Pres-de-chez-vous/Hauts-de-France/Lille/Informations-metiers/Decouvrir-les-secteurs-professionnels/La-cybersecurite).

Les métiers de l'Open Source sont les plus recherchés : 
[lire l'article du monde informatique](http://www.lemondeinformatique.fr/actualites/lire-les-metiers-de-l-open-source-sont-les-plus-recherches-50774.html),
ou encore
[Entreprises recherchent désespérément des talents Open Source](https://www.silicon.fr/entreprises-recherchent-desesperement-talents-open-source-182593.html),
et
[Open Source - des métiers pleins d'avenir](http://www.infodsi.com/articles/142673/open-source-metiers-pleins-avenir.html) ;

Administrateur(trice) de base de données : [voir la fiche métier de
ZDNET](http://www.zdnet.fr/actualites/administrateur-de-base-de-donnees-la-fiche-metier-39756701.htm) ;

À la découverte des architectures WEB front-end :
[Les sites WEB statiques](https://blog.octo.com/a-la-decouverte-des-architectures-web-front-end-1-4-les-sites-web-statiques/),
[Les Multiple Page Applications](https://blog.octo.com/a-la-decouverte-des-architectures-du-front-2-4-les-multiple-page-applications/),
[Les Single Page Applications](https://blog.octo.com/a-la-decouverte-des-architectures-du-front-3-4-les-single-page-applications/)...

Les systèmes de gestion de contenu [CMS][cms],
Les [tests](https://fr.wikipedia.org/wiki/Test_(informatique)) informatiques

[Devops](https://fr.wikipedia.org/wiki/Devops),
[DevOps, de l’intégration continue au déploiement continu][octo],
[Introduction à Gitlab CI/CD](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/),
Podcast [Qu’est-ce qu’un Ingénieur DevOps, un SysOps ou AdminSys](https://lydra.fr/es-11-quest-ce-quun-ingenieur-devops-un-sysops-ou-admin/),
[Pourquoi inclure la sécurité dans votre pipeline DevOps ?](https://connect.ed-diamond.com/MISC/MISC-087/Pourquoi-inclure-la-securite-dans-votre-pipeline-DevOps)

Les [métiers de la cybersécurité](https://www.ssi.gouv.fr/particulier/formations/profils-metiers-de-la-cybersecurite/),
Fiche Métier: [Analyste Menaces Cyber](https://www.michaelpage.fr/advice/metiers/systemes-dinformation/fiche-m%C3%A9tier-analyste-menaces-cyber),
[FindBugs](https://fr.wikipedia.org/wiki/FindBugs)

Informatique de gestion : quels métiers pour quels profils ? - [lire
l'article du journal du
net](http://www.journaldunet.com/solutions/emploi-rh/informatique-de-gestion-et-bts-sio.shtml),
sur les métiers associés au BTS SIO (rédigé avant la révision) ;

Ce qui m’a motivé dans mon parcours d’artisan développeur : [témoignage
blog de Camille
Roux](https://blog.humancoders.com/ce-qui-ma-motive-dans-mon-parcours-dartisan-developpeur-2372/) ;

[Kompass](http://fr.kompass.com/) est un annuaire d'entreprises professionnel.


## Métiers SLAM retenus dans le référentiel du BTS SIO

Les dénominations d’emplois concernées sont diverses.
À titre indicatif, les appellations les plus fréquentes sont les suivantes
(classées par ordre alphabétique) :

* analyste d'applications ;
* analyste d'études ;
* analyste programmeur ;
* chargé d'études informatiques ;
* développeur d'applications informatiques ;
* développeur d’applications mobiles ;
* technicien développement – exploitation ;
* développeur Web ;
* informaticien d'études ;
* programmeur d'applications ;
* responsable des services applicatifs ;
* technicien d'études informatiques.

[fiche]: https://framagit.org/S2SIO/metiers/-/wikis/enseignant-en-informatique
[octo]: https://blog.octo.com/devops-de-lintegration-continue-au-deploiement-continu/
[esn]: https://fr.wikipedia.org/wiki/Entreprise_de_services_du_numérique
[cms]: https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu
[markdown]: https://enacit.epfl.ch/cours/markdown-pandoc/#syntaxe-de-base-du-langage-markdown
